# Tarea | Mongodb 101

Tarea para sentar las bases de las bases de datos no relacionales.

## Instrucciones 📋

En esta tarea vamos a utilizar una colección de calificaciones de estudiantes, todas las respuestas deben de colocarse en un archivo llamado tarea_1.js y debe subirse a gitlab (siendo público el repositorio), utilizando comentarios, deben colocarse las preguntas, luego los comandos utilizados y de nuevo en comentarios las respuestas, al finalizar debe entregarse la liga de gitlab con la tarea.

1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?

3) Encuentra todas las calificaciones del estudiante con el id numero 4.

4) ¿Cuántos registros hay de tipo exam?

5) ¿Cuántos registros hay de tipo homework?

6) ¿Cuántos registros hay de tipo quiz?

7) Elimina todas las calificaciones del estudiante con el id numero 3

8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

10) A qué estudiante pertenece esta calificación.

¡Suerte!

### Pre-requisitos 🛠️

Lanzar el contenedor de mongo:
```
sudo docker run -dti -p27017:27017 mongo
```

### Pasos 🔧
* Descargar el documento grades.sjon
* Conectar la base de datos con el contenedor previamente creado
* Abrir la consola de mongo mediante el docker:
```
sudo docker exec -ti <nombre_del_contenedor> mongo
```
* Utilizar la base students en la consola de mongo
* Correr los comandos correspondientes a cada ejercicio*


## Ejecutando comandos 📄

![alt text](https://lh3.googleusercontent.com/n0fw0RrA9jKmHg5xumhXdDDgR5IekDiDhAbONXEbhpe03E9lNsZ4495zopTTnC4WO2XnrnI6IWf-YhzYMIeiAHyPqUhDfCGNst8cSIVxvdB31g9kis_QONBvbteI85N8YShpuzKCfRMBNM5pHwnxNazxF8K2BwKlmFT8syAKCnhhgr4p2X6onSumolBUHf5F9T0DyGuo9yMOx-elps2-gZV66s0ymX5n92CDGl6ipvkhNZL5bfTxkalxnMWPp9x6M161gTPOx65UTgjf0IqwnkM22RAnOx3ie1-pDGKzsrw7xHX1c0vfJgxZlUIGmHsxnsQf09vr9NfbyXOFaOcwmuBPSJgop4tGqCyE0iopDYW_2pC1TIwEGbPIeyhA8bL7HxG138VsdnISmeT0UqAQh5nREbXnu13f10WHPebO44rpeTS-hUsZNuW1JmtSyUqfvv0M-oYPhZ0zszE7wKWnCJF5eFRqXHMop6m7Wo_RDlh5oj638omEAuz0BKxRd161MqKIBxHYuIF801p4ROicT9XKe73JEd5aFqYYaDKKkujjq8uZIQ4Ch8m4vLKtk_cHTPOk9NXYnkcgHycD1WyWf9VnIJujGmDRwmgN5WSjwlGN06vq2bmALvP6YR9lnAKeLNMJOsEKLpQSwVNeJ17TC8NrUcfALYX3AxJsmFuR7aUIbM6SRvNh9MdmA1MY5ZSgn_oQUrVM5LPhhU3pDf6cwg=w1438-h733-no?authuser=0)
![alt text](https://lh3.googleusercontent.com/k4L1oIlMJPaWxZ0HrNJbuoxzgrI39nVc6lHlc_em8T0pieSknTqZmTJXCghDcZlIfMRVcEGGdqTuYAkDyYEUwQUgp71d6ewlV9Q_kIw0aw43Vqig-KVCmgWuNoginlGlsMydyvb6Ris-fXROVfU8Nm-zTZyXHwcC3P8hopjdwZlKtmaeH1uxfK30BRaEqWvStgyERSTrBd57U49aJ-4swbNzB2qD6KMDAMkFt6zyV2dQEuUoVilIC-JA0FppV6Uo5l2hC48LfhhwgEuERSCNbBLxjlD8khrpR8qXvJsg1WhnXIjcalmf1dvIJoSFUVvg4jd9WxEEYuq8nxx2NkRwbJoV_S-NyhcUMnKueuIv1UIoLauUQwL8S3vsqhHhMJBr7bertcaui70SnUhR2kszheJ9JPpdmEyX4m7_AoV6_4Mdv34Epm6_je05cpNdkU3zUp7fIsicU95CMZtKAgK3LdQ7EOQ2o_HHmihT00T5iP9TwTVr108Y2Q8maqyWhiBXmrxEaiUN5yT78MrUiiwW3x5nkNnenuN8n0gFUW1Q2m-ryYJTk6o2ijEFrzuFOIa9EHHqW9i15s_x8nWmmsp5ObfTz8BhKoA-EEg_rxhHvxl-Idh_d4kO0zh_qLbfWR8_PttUzyB0H5_sPvVIZgrCfIe3SVxg4y0vDLkrP7bpdvuw7CUuri-vG3Cxp_s-URaK647wG2lis9GKek40TJql4Q=w1439-h46-no?authuser=0)


## Autor

- Daniel Alberto Cota Ochoa     ***329701***
    - [Gitlab](https://gitlab.com/daniel_cota)
