//1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

/*2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los 
siguientes comandos en la terminal de mongo: >use students; >db.grades.count() 
¿Cuántos registros arrojo el comando count?*/
//800

// 3) Encuentra todas las calificaciones del estudiante con el id numero 4.
db.grades.find({"student_id":4});
/* Retornó:
{ "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 }
*/

// 4) ¿Cuántos registros hay de tipo exam?
//200
db.grades.find({"type":"exam"}).count();

// 5) ¿Cuántos registros hay de tipo homework?
//400
db.grades.find({"type":"homework"}).count();

// 6) ¿Cuántos registros hay de tipo quiz?
//200
db.grades.find({"type":"quiz"}).count();

// 7) Elimina todas las calificaciones del estudiante con el id numero 3
//
db.grades.updateMany({"student_id":3}, {$unset: {score:""}});
//Retornó { "acknowledged" : true, "matchedCount" : 4, "modifiedCount" : 4 }

// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
//El estudiante con el ID = 9
db.grades.find({type:"homework", "score":75.29561445722392});

// 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
//
db.grades.updateOne({"_id":ObjectId("50906d7fa3c412bb040eb591")}, {$set: {"score":100}});
//Retornó { "acknowledged" : true, "matchedCount" : 1, "modifiedCount" : 1 }

// 10) A qué estudiante pertenece esta calificación.
//Al estudiante con el ID = 6
db.grades.find({"_id":ObjectId("50906d7fa3c412bb040eb591")});
//Retornó { "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }

